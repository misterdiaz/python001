def sumar(a, b):
    suma = a + b
    return suma


def restar(a, b):
    return a - b


def multiplicar(a, b):
    return a * b


def dividir(a, b):
    return a / b


def sumar_varios(num1, *numeros):
    suma = num1
    for numero in numeros:
        suma += numero

    return suma

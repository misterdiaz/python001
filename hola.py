print("Hola mundo!!")

"""
 Esto es un comentario
 de multiples lineas
"""
mi_numero = 458

mi_flotante = 35.8 # Esto es un comentario simple de una sola linea

mi_cadena = "Hola Mundo!"

print(mi_cadena[5:-1])

print("Division:")
print( mi_numero / mi_flotante )

print("parte entera de la division:")
print( mi_numero // mi_flotante )

print("parte flotante de la division")
print( mi_numero % mi_flotante )

print("Por favor ingrese valor de a:")
a = input()
print("Por favor ingrese valor de b:")
b = input()
print(a + b)
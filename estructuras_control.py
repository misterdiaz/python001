# -*- coding: utf-8 -*-

if(True):
    print("Feliz cumpleaños")


# asignacion multiple

a, b, c = "Es un string", 45, 89

print(a, b, c)

tupla = (1983, "Nacimiento")
anio, texto = tupla

print(anio)
print(texto)

semaforo = "verde"

if semaforo == "verde":
    print("cruzar la calle")
else:
    print("esperar")

semaforo = "rojo"

print("cruzar la calle") if semaforo=="verde" else print("esperar") # ejemplo de operador ternario


"""
    Ciclo while
"""


anio = 2016
while anio > 2012:
    print(anio) #imprimimos el anio para luego restarle uno
    anio -= 1 # esto es igual a decir anio = anio - 1

"""
 Bucle for
"""

semana = ['Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado', 'Domingo']

print("Semana utilizando for simple:")
for dia in semana:
    print(dia)

print("Semana utilizando for con range: ")
for i in range(0, len(semana)):
    print(semana[i])

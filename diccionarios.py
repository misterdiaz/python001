parciales = (8, 7, 9) # tupla con valores de parciales

notas = [9, 8.5, 8, 9.4, 10, parciales] # lista con valores de notas y tupla de parciales

"""
    diccionario que contiene los datos de una persona y una lista de notas
"""
mi_diccionario = {
    'nombre': 'Luis Diaz',
    'cedula': 16326344,
    'edad': 32,
    'twitter': '@luisdiaz',
    'notas': notas
}

print(mi_diccionario['nombre'])
print(mi_diccionario['edad'])

print(mi_diccionario['notas'])

print("Mi cedula es: %d. Mi edad es: %d" % (mi_diccionario['cedula'], mi_diccionario['edad']) )

mi_diccionario['edad'] = 33
print('Mi edad es: %d' % mi_diccionario['edad'])

del(mi_diccionario['twitter'])

print(mi_diccionario)



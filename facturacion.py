import os

resp = 'y'
listado = []
while resp == 'Y' or resp == 'y':
    os.system('clear')
    print("Por favor ingrese datos del producto: ")
    nombre = input("Ingrese nombre del producto: ")
    marca = input("Ingrese marca del producto: ")
    precio = float(input("Ingrese precio del producto: "))
    producto = {
        'nombre': nombre,
        'marca': marca,
        'precio': precio
    }
    listado.append(producto)
    resp = input("Desea ingresar otro producto: (y/n) ")

suma = 0
for prod in listado:
    suma += prod['precio']

promedio = suma / len(listado)
print("El monto total de su compra factura es de: %f $" % suma)
print("El promedio es de: %f $" % promedio)

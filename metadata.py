from osgeo import gdal
import sys
# Esto nos permite lanzar excepciones de python
gdal.UseExceptions()

try:
    src_ds = gdal.Open( "imagen/vrss-1_mss-1_0165_0173.tiff" )
    print(src_ds.RasterCount)
except(RuntimeError, e):
    print('No se logro abrir la imagen')
    print(e)
    sys.exit(1)

try:
    srcband = src_ds.GetRasterBand(1)
    stats = srcband.GetStatistics( True, True )
    print( srcband.GetMinimum() )
    print( srcband.GetHistogram() )
    print(stats)
except(RuntimeError, e):
    # for example, try GetRasterBand(10)
    print('Band ( %i ) not found') % band_num
    print(e)
    sys.exit(1)
